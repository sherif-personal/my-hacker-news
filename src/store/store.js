import Vue from 'vue'
import Vuex from 'vuex'

// Import store Modules
import storyDialog from 'store/modules/storyDialog'
import apiLoading from 'store/modules/apiLoading'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {},
  modules: {
    storyDialog,
    apiLoading
  }
})

export default store
