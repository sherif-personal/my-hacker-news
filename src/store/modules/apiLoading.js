export default {
  state: {
    showLoading: false,
    loadingText: ''
  },
  mutations: {
    setShowLoading (state, status) {
      state.showLoading = status
    },
    setLoadingText (state, text) {
      state.loadingText = text
    }
  },
  getters: {
    getShowLoading: state => {
      return state.showLoading
    },
    getloadingText: state => {
      return state.loadingText
    }
  }
}
