# My Hacker News

Creating my own version of Hacker News by utilizing the official Hacker News API https://github.com/HackerNews/API. 
This personal project is built using Vue JS with Webpack. Vuetify for UI Library and Vuex for application state managment.

it can be viewed here: https://sherif-personal.gitlab.io/my-hacker-news

## Installation

### Prerequisite
- node
- npm

### Installing dependencies
```bash
npm install
```

## Usage

### Start Development server
```
npm run dev # run a webpack dev server for application
```

### Build development
```
npm run build # build dist folder
```
