import axios from 'axios'

const baseAPIDomain = process.env.BASE_API_URL

export default {

  getTopStories () {
    const apiCallUrl = baseAPIDomain + 'topstories.json'
    return axios.get(apiCallUrl).then(response => response.data)
  },

  getNewStories () {
    const apiCallUrl = baseAPIDomain + 'newstories.json'
    return axios.get(apiCallUrl).then(response => response.data)
  },

  getBestStories () {
    const apiCallUrl = baseAPIDomain + 'beststories.json'
    return axios.get(apiCallUrl).then(response => response.data)
  },

  getStories (storiesIdsList) {
    const promises = []

    storiesIdsList.forEach((storyId) => {
      promises.push(axios.get(baseAPIDomain + 'item/' + storyId + '.json'))
    })

    return axios.all(promises).then((results) => results)
  },

  getComments (commentsIdsList) {
    const promises = []
    commentsIdsList.forEach((commentId) => {
      promises.push(axios.get(baseAPIDomain + 'item/' + commentId + '.json'))
    })

    return axios.all(promises).then((results) => results)
  }

}
