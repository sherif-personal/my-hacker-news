import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/lib/util/colors'

// Import Vuex store to manage application state
import vStore from 'store/store'

// Import Polyfill
import 'core-js/stable'
import 'regenerator-runtime/runtime'

// Import SCSS files
import '@/styles/main.scss'

// Import Router
import router from '@/router/router'

import App from '@/App'

// Imoport third party libraries
import moment from 'moment'

// Add Libraries to Vue
Vue.use(Vuetify)

/* eslint-disable-next-line no-new */
new Vuetify({
  theme: {
    themes: {
      light: {
        primary: colors.orange.darken1,
        secondary: colors.orange.lighten1
      }
    }
  }
})

// Vue Global filters
Vue.filter('formatDate', function (value) {
  if (value) {
    return moment.unix(String(value)).format('LLLL')
  }
})

Vue.filter('formatDateFromNow', function (value) {
  if (value) {
    return moment.unix(String(value)).fromNow()
  }
})

/* eslint-disable-next-line no-new */
new Vue({
  vuetify: new Vuetify(),
  el: '#app',
  router,
  store: vStore,
  render: h => h(App)
})

module.hot.accept()
