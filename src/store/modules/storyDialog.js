export default {
  state: {
    showModal: false,
    story: {}
  },
  mutations: {
    setStoryDialog (state, status) {
      state.showModal = status
    },
    setStoryData (state, story) {
      state.story = story
    }
  },
  getters: {
    showModal: state => {
      return state.showModal
    },
    getStoryData: state => {
      return state.storyData
    }
  }
}
