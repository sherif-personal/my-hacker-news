import VueRouter from 'vue-router'
import Vue from 'vue'

// Import Page components for routes
import TopStories from 'pages/TopStories'
import NewStories from '../pages/NewStories'
import BestStories from 'pages/BestStories'

const routes = [
  {
    path: '/',
    component: TopStories
  },
  {
    path: '/top-stories',
    component: TopStories
  },
  {
    path: '/new-stories',
    component: NewStories
  },
  {
    path: '/best-stories',
    component: BestStories
  },
  {
    path: '*',
    redirect: '/top-stories'
  }
]

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export { router as default, routes }
